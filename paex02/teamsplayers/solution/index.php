<?php
require "model.php";

foreach($model as $teamName=>$team){
   echo "<div style='border:1px solid black;padding:4px;margin:4px;'><a href='?team={$teamName}'>{$teamName}</a></div>";
}

if(isset($_GET['team'])){
  $selectedTeam = $model[$_GET['team']];
  echo "<strong>Selected team:</strong> "; 
  echo $_GET['team'];
  echo "<br><strong>Players:</strong><br>";
  echo implode(",",$selectedTeam);
}
