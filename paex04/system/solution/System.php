<?php
class System {
	public $menu = [];
	public static function getSystem($path){
		$res = new System;
		$f = file($path);
		foreach($f as $row){
			$rowParts = explode("=",trim($row)); 
			$fieldName = $rowParts[0];
			if($fieldName=="menu"){
				$menuItems = explode(";",$rowParts[1]);
				foreach($menuItems as $menuItem){
					$menuItemKvp = explode("|",$menuItem);
					$res->menu[$menuItemKvp[0]] = $menuItemKvp[1];
				}
			} else {
				$res->$fieldName = $rowParts[1];
			}
		}
		return $res;
	}
	public function setSystem($path){
		$f = fopen($path,"w");
		$fieldsCount = count(get_object_vars($this));
		$counter = 0;
		foreach($this as $k=>$v){
			if($k=="menu"){ 
				$out = "";
				foreach($v as $k=>$v){
					$out.=$k."|".$v.";";
				}
				$out = rtrim($out,";"); 
				fwrite($f,$k."=".$out);
				
			} else {
				fwrite($f,$k."=".$v);
			}
			if($counter++<$fieldsCount-1){
				fwrite($f,PHP_EOL);
			}
		}
		
		fclose($f);
	}
}