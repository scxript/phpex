<?php
define("MORE",1);
define("LESS",2);
require "model.php";
$allowedCards = [1,2,3,4,5,6,7,8,9,10,12,13,14];
session_start();
if(!isset($_SESSION['cards'])||empty($_SESSION['cards'])){
	$_SESSION['cards'] 	= array($allowedCards[mt_rand(0,sizeof($allowedCards)-1)]);
	$_SESSION['balance'] 	= mt_rand(0,100);
}    
$sessionCards	= $_SESSION['cards']; 
$lastCard	= $sessionCards[sizeof($sessionCards)-1]; 
$restart	= false;
if(isset($_GET['c'])&&in_array($_GET['c'],[MORE,LESS])){
	$newCard = $allowedCards[mt_rand(0,sizeof($allowedCards)-1)];  
	$_SESSION['cards'][] = $newCard;
	$sessionCards	= $_SESSION['cards'];
	$c = $_GET['c'];
	if(($c==MORE && $newCard>$lastCard)||($c==LESS && $newCard<$lastCard)){
		$_SESSION['balance'] += $_SESSION['balance']; 
	} else {
		$_SESSION['balance'] 	= 0; 
		$restart		= true;
	}
} 
$balance = $_SESSION['balance']; 
$posx = 0;
foreach($sessionCards as $cardIndex){ 
	echo "<div style='position:absolute;left:{$posx}px;overflow:hidden;width:90px;height:130px;background-image:url(cards.png);background-position: -{$cards[$cardIndex][0]}px -{$cards[$cardIndex][1]}px'></div>";
	$posx+=20;
} 
?>  
<div style='position:absolute;top:150px;'>
	Your Balance : <?=$balance?>
	<br>
	<?php
	if($restart){
		$_SESSION['cards'] 	= [];
		$_SESSION['balance'] 	= 0;
		echo "<a href='index.php'>RESTART</a>";
	} else {
		echo "<a href='?c=".MORE."'>HIGHER</a> ";
		echo "<a href='?c=".LESS."'>LOWER</a>";
	} ?>
</div> 
