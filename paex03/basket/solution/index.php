<style>
.basket {
	background-image:url(ic_shopping_basket_black_24dp_2x.png);
	background-size:24px 24px;
	height:24px;
	width:24px;
	background-repeat:no-repeat;
	display:block;
}
</style>
<?php
session_start();
require "model.php";  
if(isset($_GET['card'])){
 	$pid = $_GET['card'];
	if(!isset($_SESSION['card'])){
		$_SESSION['card'] = [];
	}
	if(!isset($_SESSION['card'][$pid])){
		$_SESSION['card'][$pid] = 1;
	} else {
		$_SESSION['card'][$pid] += 1;
	} 
	header("location: index.php");
} 

echo "<table border=1>";
echo "<tr><td>#</td><td>Product</td><td>Picture</td><td>Price</td><td></td></tr>";
foreach($products as $k=>$v){
	echo "<tr><td>{$k}</td><td>{$v['name']}</td><td><img width=100 src='{$v['picture']}' /></td><td>{$v['price']}</td><td><a class='basket' href='?card={$k}' /></td></tr>";
}
echo "</table>";
$productsInBasket = 0;
if(isset($_SESSION['card'])){
	foreach($_SESSION['card'] as $v){
		$productsInBasket+=$v;
	}
} 
if($productsInBasket){
	echo "Products in basket {$productsInBasket} <a href='card.php'>Show card</a>"; 
}

